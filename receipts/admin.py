from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt


# Register your models here.
# I think all my list displays are wrong or incomplete.
# I just added stuff that seemed to make sense.
@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "number",  # do I need this?
    )


@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "tax",
        "date",
        "id",
    )
